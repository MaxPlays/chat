package saver;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

/**
* Created by Max_Plays on 19. Okt. 2016
*/
public class Adder extends JFrame {

	private static final long serialVersionUID = 8573511696902586040L;
	private JPanel contentPane;
	private JTextField name;
	private JTextField ip;
	private Adder frame;
	public Adder(String pre) {
		setResizable(false);
		setTitle("Eintrag hinzuf\u00FCgen");
		start(pre);
	}

	public void start(String pre){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 279, 140);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		setLocationRelativeTo(null);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		frame = this;
		
		name = new JTextField();
		name.setBounds(55, 11, 189, 20);
		contentPane.add(name);
		name.setColumns(10);
		name.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ip.requestFocus();
			}
		});
		 
		name.setDocument(new JTextFieldLimit(25));
		
		ip = new JTextField();
		ip.setBounds(55, 42, 189, 20);
		contentPane.add(ip);
		ip.setColumns(10);
		ip.setText(pre);
		ip.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!name.getText().trim().equals("") & !ip.getText().trim().equals("")){
					Save s = new Save(name.getText(), ip.getText());
					switch(s.save()){
					case ADDED:
						dispose();
						break;
					case FAILED:
						JOptionPane.showMessageDialog(frame, "Ein Fehler ist aufgetreten", "Fehler", JOptionPane.ERROR_MESSAGE);
						dispose();
						break;
					case NAME_EXISTS:
						JOptionPane.showMessageDialog(frame, "Der Name ist bereits vergeben", "Fehler", JOptionPane.ERROR_MESSAGE);
						break;
					default:
						dispose();
						break;
					}
				}else{
					JOptionPane.showMessageDialog(frame, "Du musst beide Felder ausf�llen", "Fehler", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 14, 46, 14);
		contentPane.add(lblName);
		
		JLabel lblAdresse = new JLabel("Adresse");
		lblAdresse.setBounds(10, 45, 46, 14);
		contentPane.add(lblAdresse);
		
		JButton save = new JButton("Speichern");
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!name.getText().trim().equals("") & !ip.getText().trim().equals("")){
					Save s = new Save(name.getText(), ip.getText());
					switch(s.save()){
					case ADDED:
						dispose();
						break;
					case FAILED:
						JOptionPane.showMessageDialog(frame, "Ein Fehler ist aufgetreten", "Fehler", JOptionPane.ERROR_MESSAGE);
						dispose();
						break;
					case NAME_EXISTS:
						JOptionPane.showMessageDialog(frame, "Der Name ist bereits vergeben", "Fehler", JOptionPane.ERROR_MESSAGE);
						break;
					default:
						dispose();
						break;
					}
				}else{
					JOptionPane.showMessageDialog(frame, "Du musst beide Felder ausf�llen", "Fehler", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		save.setBounds(55, 73, 189, 23);
		contentPane.add(save);
		
		
		
		
		setVisible(true);
	}
}
