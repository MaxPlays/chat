package saver;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.prefs.Preferences;

import client.cgui;

/**
* Created by Max_Plays on 19. Okt. 2016
*/
public class Save implements Serializable{

	private static final long serialVersionUID = 3619115465760412994L;
	private String name, ip;
	private static HashMap<String, Save> saves = new HashMap<>();
	
	public Save(String name, String ip){
		this.ip = ip;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public Result save(){
		if(!nameExists(name)){
			if(!ipExists(ip)){
				saves.put(name, this);
				if(cgui.saves != null){
					cgui.add(this);
				}
				saveFile();
				return Result.ADDED;
			}else{
				try{
					Save s = getSaveByIp(ip);
					cgui.remove(s);
					save();
				}catch(NullPointerException ex){
					return Result.FAILED;
				}
				return Result.NAME_CHANGED;
			}
		}else{
			return Result.NAME_EXISTS;
		}
	}
	public Result delete(){
		if(nameExists(name)){
			saves.remove(name);
			saveFile();
			return Result.REMOVED;
		}
		return Result.NOT_IN_LIST;
	}
	
	public static Save getSaveByName(String name){
		if(nameExists(name))
			return saves.get(name);
		return null;
	}
	public static Save getSaveByIp(String ip){
		Save s = getSave(ip).getValue();
		if(s != null)
			return s;
		return null;
	}
	public static boolean nameExists(String name){
		return saves.containsKey(name);
	}
	public static boolean ipExists(String ip){
		return getSave(ip) != null;
	}
	public enum Result{
		NAME_EXISTS,
		NAME_CHANGED,
		ADDED,
		REMOVED,
		NOT_IN_LIST,
		FAILED
	}
	private static Entry<String, Save> getSave(String ip) throws NullPointerException{
		for(Entry<String, Save> e: saves.entrySet()){
			if(e.getValue().getIp().equalsIgnoreCase(ip))
				return e;
		}
		return null;
	}
	public static void saveFile(){
		Preferences prefs = Preferences.userNodeForPackage(Save.class);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(saves);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		prefs.put("list", Base64.getEncoder().encodeToString(baos.toByteArray()));
		try {
			baos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	public static void loadFile(){
		Preferences prefs = Preferences.userNodeForPackage(Save.class);
		

			if(prefs.get("list", "list") == null)
				return;
		
		
		ByteArrayInputStream baos = new ByteArrayInputStream(Base64.getDecoder().decode(prefs.get("list", "list")));
		ObjectInputStream oos = null;
		try {
			oos = new ObjectInputStream(baos);
			saves = (HashMap<String, Save>) oos.readObject();
			cgui.load = saves.values();
			System.out.println(saves.toString());
		}catch(EOFException e){
			
			System.out.println("No preference entry found");
			return;
		
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			oos.close();
			baos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
