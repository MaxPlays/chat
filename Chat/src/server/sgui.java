package server;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultCaret;

public class sgui extends JFrame {

	private static final long serialVersionUID = -6285049882289899573L;
	private JPanel contentPane;
	public static JLabel online;
	public static JLabel ip;
	public static JTextArea area;
	static boolean first = true;
	

	public sgui() {
		setResizable(false);
		setTitle("Server");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 615, 398);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {}
			
			@Override
			public void windowIconified(WindowEvent e) {}
			
			@Override
			public void windowDeiconified(WindowEvent e) {}
			
			@Override
			public void windowDeactivated(WindowEvent e) {}
			
			@Override
			public void windowClosing(WindowEvent e) {
				dispose();
				System.exit(0);
			}

			@Override
			public void windowActivated(WindowEvent e) {	
			}
			@Override
			public void windowClosed(WindowEvent e) {}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 13, 585, 295);
		contentPane.add(scrollPane);
		
		area = new JTextArea();
		area.setFont(new Font("Consolas", Font.PLAIN, 16));
		area.setLineWrap(true);
		area.setEditable(false);
		DefaultCaret caret = (DefaultCaret)area.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		scrollPane.setViewportView(area);
		
		JButton btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Server.broadcast("DISCONNECT:==:==:close");
				dispose();
				System.exit(0);
			}
		});
		btnStop.setBounds(12, 321, 143, 25);
		contentPane.add(btnStop);
		
		online = new JLabel("Online: 0");
		online.setBounds(375, 325, 56, 16);
		contentPane.add(online);
		
		ip = new JLabel("Local IP: 127.0.0.1");
		ip.setBounds(200, 325, 163, 16);
		contentPane.add(ip);
		setLocationRelativeTo(null);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}
		
		
		setVisible(true);
		
		new Server(true);
	}
	public static void println(String line){
		String time = new SimpleDateFormat("[dd.MM.yyyy HH:mm] ").format(new Date(System.currentTimeMillis()));
		if(!first){
			area.setText(area.getText() + "\n" + time + line);
		}else{
			first = false;
			area.setText(time + line);
		}
	}
}
