package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;

import main.Encrypt;

/**
* Created by Max_Plays on 16. Okt. 2016
*/
public class Server {

	public static ServerSocket s;
	public static ArrayList<PrintWriter> map = new ArrayList<>();
	public static ArrayList<String> names = new ArrayList<>();
	public static boolean gui;
	
	@SuppressWarnings("static-access")
	public Server(boolean gui){
		try{
			
			s = new ServerSocket(4815);
			
			String time = new SimpleDateFormat("[dd.MM.yyyy HH:mm] ").format(new Date(System.currentTimeMillis()));
			
			System.out.println(time + "Der Server wurde gestartet\n");
			if(gui)
				sgui.println("Der Server wurde gestartet\n");
			
			new onlineSender().start();
			new listener().start();
			this.gui = gui;
			if(gui){
				sgui.ip.setText("Local IP: " + getIpAddress());
			}
			
		}catch(Exception e){
			String time = new SimpleDateFormat("[dd.MM.yyyy HH:mm] ").format(new Date(System.currentTimeMillis()));
			System.out.println(time + "Der Server konnte nicht gestartet werden\n");
			if(gui)
				sgui.println("Der Server konnte nicht gestartet werden\n");
			e.printStackTrace();
			return;
		}
	}
	public class ClientListener extends Thread{
		
		private BufferedReader reader;
		private PrintWriter pw;
		private Socket c;
		
		public ClientListener(Socket client, PrintWriter pw){
			try {
				reader = new BufferedReader(new InputStreamReader(client.getInputStream(), StandardCharsets.UTF_8));
				this.pw = pw;
				c = client;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		@Override
		public void run(){
			String msg;
			
			try {
				while((msg = reader.readLine()) != null){
					msg = Encrypt.decrypt(msg);
					if(!msg.contains(":") & msg.contains(" ist dem Chat beigetreten")){
						String name = msg.replace(" ist dem Chat beigetreten", "").toLowerCase().trim();
						if(names.contains(name)){
							pw.write("DISCONNECT:==:==:name");
							pw.flush();
							c.close();
							map.remove(pw);
							String time = new SimpleDateFormat("[dd.MM.yyyy HH:mm] ").format(new Date(System.currentTimeMillis()));
							System.out.println(time + "Die Verbindung des Clients wurde getrennt, da der Benutzername bereits verwendet wird");
							if(gui)
								sgui.println("Die Verbindung des Clients wurde getrennt, da der Benutzername bereits verwendet wird");
							interrupt();
						}else{
							names.add(name);
						}
					}
					if(!msg.contains(":") & msg.contains(" hat seine Verbindung getrennt")){
						String name = msg.replace(" hat seine Verbindung getrennt", "").toLowerCase().trim();
						names.remove(name);
					}
					if(msg.startsWith("DISCONNECT:==:==:client")){
						map.remove(pw);
						c.close();
						interrupt();
						return;
					}
					broadcast(msg);
					String time = new SimpleDateFormat("[dd.MM.yyyy HH:mm] ").format(new Date(System.currentTimeMillis()));
					if(!msg.startsWith("FILE:==:==:"))
						System.out.println(time + msg);
					if(gui & !msg.startsWith("FILE:==:==:"))
						sgui.println(msg);
				}
			}catch(SocketException e){
				map.remove(pw);
				interrupt();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	public static void broadcast(String message){
		for(PrintWriter pw: map){
			pw.println(Encrypt.encrypt(message));
			pw.flush();
		}
	}
	public void listen(){
		while(true){
			try {
				Socket so = s.accept();
				PrintWriter pw = new PrintWriter(new OutputStreamWriter(so.getOutputStream(), StandardCharsets.UTF_8));
				map.add(pw);
				new ClientListener(so, pw).start();
			} catch (IOException e) {
				e.printStackTrace();
			}		
		}
	}
	public class listener extends Thread{
		
		@Override
		public void run(){
			listen();
		}
		
	}
	public class onlineSender extends Thread{
		
		@Override
		public void run(){
			while(true){
				String online = "";
				for(String s: names){
					online = online + s + ":";
				}
				for(PrintWriter pw: map){
					pw.println(Encrypt.encrypt("ONLINE:==:==:" + map.size() + ":==:==:" + online));
					pw.flush();
				}
				if(gui)
					sgui.online.setText("Online: " + map.size());
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	public static String getIpAddress() { 
        try {
            for (Enumeration<?> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                for (Enumeration<?> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = (InetAddress) enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()&&inetAddress instanceof Inet4Address) {
                        String ipAddress=inetAddress.getHostAddress().toString();
                        return ipAddress;
                    }
                }
            }
        } catch (SocketException ex) {
        }
        return null; 
}
}
