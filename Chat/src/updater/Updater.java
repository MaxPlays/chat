package updater;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import main.Chat;

/**
* Created by Max_Plays on 20. Okt. 2016
*/
public class Updater {

	public Updater(){
		
		
		int newest = Chat.release;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new URL(Chat.version).openConnection().getInputStream()));
			newest = Integer.valueOf(br.readLine());
			br.close();
		} catch (NumberFormatException | IOException e2) {
			e2.printStackTrace();
		}
		
		
		if(Chat.release < newest){
			
			JFrame frame = new JFrame();
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
					| UnsupportedLookAndFeelException e1) {
				e1.printStackTrace();
			}
			int select = JOptionPane.showConfirmDialog(frame, "Ein Update ist verf�gbar. M�chtest du es installieren?");
			frame.dispose();
			if(select != 0){
				return;
			}
			try {				
				BufferedInputStream bis = new BufferedInputStream(new URL(Chat.download).openConnection().getInputStream());
				FileOutputStream fos = new FileOutputStream(new File(System.getProperty("java.class.path")).getName(), true);
				
				byte[] buffer = new byte[1024];
				int read;
				while((read = bis.read(buffer, 0, buffer.length)) != -1){
					fos.write(buffer, 0, read);
				}
				fos.flush();
				fos.close();
				bis.close();
				
				JFrame frame1 = new JFrame();
				JOptionPane.showMessageDialog(frame1, "Das Update wurde heruntergeladen und installiert!");
				
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
	
	
}
