package client;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultCaret;

import main.Ser;
import saver.Adder;
import saver.Save;

/**
* Created by Max_Plays on 16. Okt. 2016
*/
public class cgui extends JFrame {

	private static final long serialVersionUID = -2771949387133277354L;
	private static JPanel contentPane;
	public static JTextField input;
	private static JTextArea area;
	private static boolean first = true;
	private JScrollPane scrollPane;
	public static JTextField user;
	public static JButton connect;
	private static Client c;
	public static JLabel online;
	public static cgui frame;
	public static JTextField server;
	public static JButton datei;
	private JScrollPane scrollPane_1;
	public static JList<String> saves;
	public static List<String> save = new ArrayList<>();
	public static Collection<Save> load = new ArrayList<>();

	//s
	
	public cgui() {
		setResizable(false);
		setTitle("Chat");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 885, 627);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		frame = this;
		
		input = new JTextField();
		input.setFont(new Font("Calibri", Font.PLAIN, 18));
		input.setBounds(10, 462, 768, 33);
		contentPane.add(input);
		input.setColumns(10);
		input.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!input.getText().trim().equals("")){
					if(!user.isEditable()){
						c.sendMessage(input.getText(), true);
						input.setText("");
						input.requestFocus();
					}else{
						input.setText("");
					}
				}
			}
		});
		
		JButton send = new JButton("Senden");
		send.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!input.getText().trim().equals("")){
					if(!user.isEditable()){
						c.sendMessage(input.getText(), true);
						input.setText("");
						input.requestFocus();
					}else{
						input.setText("");
					}
				}
			}
		});
		send.setBounds(780, 462, 79, 33);
		contentPane.add(send);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 849, 440);
		contentPane.add(scrollPane);
		
		area = new JTextArea();
		scrollPane.setViewportView(area);
		area.setEditable(false);
		area.setLineWrap(true);
		area.setFont(new Font("Consolas", Font.PLAIN, 16));
		DefaultCaret caret = (DefaultCaret)area.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		user = new JTextField();
		user.setBounds(97, 530, 113, 20);
		contentPane.add(user);
		user.setColumns(10);
		user.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				connect();
			}
		});
		
		JLabel lblBenutzername = new JLabel("Benutzername:");
		lblBenutzername.setLabelFor(user);
		lblBenutzername.setBounds(10, 533, 93, 14);
		contentPane.add(lblBenutzername);
		
		connect = new JButton("Verbinden");
		connect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connect();
			}
		});
		connect.setBounds(10, 559, 113, 23);
		contentPane.add(connect);
		
		JLabel lblOnline = new JLabel("Online:");
		lblOnline.setBounds(292, 563, 46, 14);
		contentPane.add(lblOnline);
		lblOnline.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if(!user.isEditable())
					JOptionPane.showMessageDialog(frame, c.on, "Clients online", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		
		online = new JLabel("0");
		online.setBounds(332, 563, 46, 14);
		contentPane.add(online);
		
		JLabel lblServer = new JLabel("Server:");
		lblServer.setBounds(10, 507, 56, 16);
		contentPane.add(lblServer);
		
		server = new JTextField();
		server.setColumns(10);
		server.setBounds(97, 506, 113, 20);
		server.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				connect();
			}
		});
		contentPane.add(server);
		
		datei = new JButton("Datei");
		datei.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(user.isEditable())
					return;
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle("Datei Ausw�hlen");
                fc.setFileSelectionMode(0);
                fc.setApproveButtonText("Senden");
                Action details = fc.getActionMap().get("viewTypeDetails");
                details.actionPerformed(null);
                fc.setMultiSelectionEnabled(false);
                fc.showOpenDialog(null);
                
                if (fc.getSelectedFile() != null) {
                	File file = fc.getSelectedFile();
                	String base = Ser.toBase64(file);
                	c.sendMessage(cgui.user.getText() + " hat die Datei " + file.getName() + " gesendet", false);
                	c.sendMessage("FILE:==:==:" + user.getText() + ":==:==:" + file.getName() + ":==:==:" + base, false);
                }
			}
		});
		datei.setBounds(780, 502, 79, 33);
		contentPane.add(datei);
		
		JButton disconnect = new JButton("Verbindung trennen");
		disconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!user.isEditable()){
					c.sendMessage(user.getText() + " hat seine Verbindung getrennt", false);
					c.sendMessage("DISCONNECT:==:==:client", false);
					c.sl.interrupt();
					try {
						c.s.close();
						c.reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					c.writer.close();
					connect.setEnabled(true);
					user.setEditable(true);
					server.setEditable(true);	
					input.setText("");
					online.setText("0");
					
				}
			}
		});
		disconnect.setBounds(133, 559, 146, 23);
		contentPane.add(disconnect);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(385, 506, 184, 76);
		contentPane.add(scrollPane_1);
		
		saves = new JList<String>();
		saves.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		for(Save s: load){
			addFirst(s);
		}
		saves.addMouseListener(new MouseAdapter(){
		      public void mouseClicked(MouseEvent evt){
		        if ((evt.getClickCount() == 2) && (saves.getSelectedIndex() >= 0)){
		        	if(saves.getSelectedValue() == null)
						return;
					if(user.isEditable()){
						String username = JOptionPane.showInputDialog(frame, "Benutzername", "Benutzername angeben", JOptionPane.QUESTION_MESSAGE);
						user.setText(username);
						server.setText(Save.getSaveByName(saves.getSelectedValue()).getIp());
						
						user.setEditable(false);
						connect.setEnabled(false);
						server.setEditable(false);	
						input.requestFocus();
						c = new Client(user.getText(), Save.getSaveByName(saves.getSelectedValue()).getIp());
					}else{
						c.sendMessage(user.getText() + " hat seine Verbindung getrennt", false);
						c.sendMessage("DISCONNECT:==:==:client", false);
						c.sl.interrupt();
						try {
							c.s.close();
							c.reader.close();
						} catch (IOException ex) {
							ex.printStackTrace();
						}
						c.writer.close();
						
						input.setText("");
						online.setText("0");
						
						String username = JOptionPane.showInputDialog(frame, "Bitte gib einen Benutzernamen an", "Benutzername angeben", JOptionPane.QUESTION_MESSAGE);
						
						server.setText(Save.getSaveByName(saves.getSelectedValue()).getIp());
						user.setText(username);
						user.setEditable(false);
						connect.setEnabled(false);
						server.setEditable(false);	
						input.requestFocus();
						c = new Client(username, Save.getSaveByName(saves.getSelectedValue()).getIp());
					}
				
		        }
		      }
		    });
		scrollPane_1.setViewportView(saves);
		
		JButton saveConnect = new JButton("Verbinden");
		saveConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(saves.getSelectedValue() == null)
					return;
				if(user.isEditable()){
					String username = JOptionPane.showInputDialog(frame, "Benutzername", "Benutzername angeben", JOptionPane.QUESTION_MESSAGE);
					user.setText(username);
					server.setText(Save.getSaveByName(saves.getSelectedValue()).getIp());
					
					user.setEditable(false);
					connect.setEnabled(false);
					server.setEditable(false);	
					input.requestFocus();
					c = new Client(user.getText(), Save.getSaveByName(saves.getSelectedValue()).getIp());
				}else{
					c.sendMessage(user.getText() + " hat seine Verbindung getrennt", false);
					c.sendMessage("DISCONNECT:==:==:client", false);
					c.sl.interrupt();
					try {
						c.s.close();
						c.reader.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
					c.writer.close();
					
					input.setText("");
					online.setText("0");
					
					String username = JOptionPane.showInputDialog(frame, "Bitte gib einen Benutzernamen an", "Benutzername angeben", JOptionPane.QUESTION_MESSAGE);
					
					server.setText(Save.getSaveByName(saves.getSelectedValue()).getIp());
					user.setText(username);
					user.setEditable(false);
					connect.setEnabled(false);
					server.setEditable(false);	
					input.requestFocus();
					c = new Client(username, Save.getSaveByName(saves.getSelectedValue()).getIp());
				}
			
			}
		});
		saveConnect.setBounds(578, 552, 105, 25);
		contentPane.add(saveConnect);
		
		JButton delete = new JButton("L\u00F6schen");
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(saves.getSelectedValue() != null){
					remove(Save.getSaveByName(saves.getSelectedValue()));
				}
			}
		});
		delete.setBounds(579, 529, 104, 23);
		contentPane.add(delete);
		
		JButton add = new JButton("Hinzuf\u00FCgen");
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(user.isEditable()){
					new Adder("");
				}else{
					new Adder(server.getText());
				}
			}
		});
		add.setBounds(579, 506, 104, 23);
		contentPane.add(add);
		
		
		addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
            	if(!user.isEditable())	
                	c.sendMessage(user.getText() + " hat seine Verbindung getrennt", false);
            	e.getWindow().dispose();
            }
        });
		
		setVisible(true);
	}
	public static void println(String line){
		if(!first){
			area.setText(area.getText() + "\n" + line);
		}else{
			first = false;
			area.setText(line);
		}
	}
public void connect(){
	user.setEditable(false);
	connect.setEnabled(false);
	server.setEditable(false);	
	input.requestFocus();
	c = new Client(user.getText(), server.getText());
}
public static void add(Save s){
	save.add(s.getName());
	saves.setListData(convert());
}
public static void addFirst(Save s){
	save.add(s.getName());
	saves.setListData(save.toArray(new String[0]));
}
public static void remove(Save s){
	save.remove(s.getName());
	s.delete();
	saves.setListData(convert());
}
public static String[] convert(){
	String[] entrys = new String[save.size()];
	int i = entrys.length - 1;
	int b = 0;
	while(i >= 0){
		entrys[i] = save.get(b);
		i--;
		b++;
	}
	return entrys;
}
}
