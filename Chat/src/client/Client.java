package client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

import javax.swing.JOptionPane;

import main.Encrypt;
import main.Ser;

/**
* Created by Max_Plays on 16. Okt. 2016
*/
public class Client {

	Socket s;
	BufferedReader reader;
	PrintWriter writer;
	String on = "";
	ServerListener sl;
	
	public Client(String username, String server){
		
		if(username.trim().equals("")){
			cgui.println("Du musst einen Benutzernamen eingeben");
			cgui.connect.setEnabled(true);
			cgui.user.setEditable(true);
			cgui.server.setEditable(true);			
			return;
		}
		if(server.trim().equals("")){
			cgui.println("Du musst einen Server eingeben");
			cgui.connect.setEnabled(true);
			cgui.user.setEditable(true);
			cgui.server.setEditable(true);			
			return;
		}
		try {
			
			s = new Socket(server, 4815);
			reader = new BufferedReader(new InputStreamReader(s.getInputStream(), StandardCharsets.UTF_8));
			writer = new PrintWriter(new OutputStreamWriter(s.getOutputStream(), StandardCharsets.UTF_8));
			
			sl = new ServerListener();
			sl.start();
			
			cgui.println("Verbindung hergestellt");
			
			sendMessage(cgui.user.getText() + " ist dem Chat beigetreten", false);
		} catch (Exception e) {
			cgui.println("Verbindung konnte nicht hergestellt werden");
			cgui.connect.setEnabled(true);
			cgui.user.setEditable(true);
			cgui.server.setEditable(true);	
			e.printStackTrace();
		}
	}
	public void sendMessage(String message, boolean prefix){	
		writer.println(Encrypt.encrypt((prefix ? cgui.user.getText() + ": " : "") + message));
		writer.flush();
		if(!message.startsWith("FILE:==:==:") & !message.startsWith("DISCONNECT:==:==:"))
			cgui.println((prefix ? "Du: " : "") + message);
	}
	
	public class ServerListener extends Thread{
		
		@Override
		public void run(){
			String msg;
			
			try{
				while((msg = reader.readLine()) != null){
					msg = Encrypt.decrypt(msg);
					if(msg.startsWith("ONLINE:==:==:")){
						int online = Integer.valueOf(msg.split(":==:==:")[1]);
						cgui.online.setText(online + "");
						on = msg.split(":==:==:")[2].replace(":", "|");
						continue;
					}
					if(msg.startsWith("DISCONNECT:==:==:name")){
						cgui.println("Deine Verbindung wurde getrennt, da der Benutzername bereits verwendet wird");
						cgui.connect.setEnabled(true);
						cgui.user.setEditable(true);
						cgui.server.setEditable(true);	
						cgui.input.setText("");
						cgui.online.setText("0");
						s.close();
						interrupt();
						return;
					}
					if(msg.startsWith("DISCONNECT:==:==:close")){
						cgui.println("Der Server wurde geschlossen");
						cgui.connect.setEnabled(true);
						cgui.user.setEditable(true);
						cgui.server.setEditable(true);	
						cgui.input.setText("");
						cgui.online.setText("0");
						s.close();
						interrupt();
						return;
					}
					if(msg.startsWith("FILE:==:==:")){
						String[] array = msg.split(":==:==:");
						if(!array[1].equalsIgnoreCase(cgui.user.getText())){
							
						int i = JOptionPane.showConfirmDialog(cgui.frame, array[1] + " m�chte die Datei " + array[2] + " schicken", "Datei empfangen", JOptionPane.YES_NO_OPTION);
						
						if(i == 0){
							FileOutputStream fos = new FileOutputStream(new File(array[2]));
							fos.write(Ser.fromBase64(msg.split(":==:==:")[3]));
							fos.close();
						}
						}
						continue;
					}
					
					if(!msg.startsWith(cgui.user.getText().trim()) & !msg.contains("FILE:==:==:") & !msg.startsWith("DISCONNECT:==:==:"))
						cgui.println(msg);
				}
			}catch(SocketException e){
				
				cgui.println("Die Verbundung zum Server wurde unterbrochen");
				cgui.connect.setEnabled(true);
				cgui.user.setEditable(true);
				cgui.server.setEditable(true);	
				cgui.input.setText("");
				cgui.online.setText("0");
			
			}catch(IOException e){	
				e.printStackTrace();
			}
		}
		
	}
}

