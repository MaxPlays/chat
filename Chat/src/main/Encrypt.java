package main;
import java.util.HashMap;
import java.util.Random;

public class Encrypt {

	static HashMap<Integer, String> is = new HashMap<>();
	static HashMap<String, Integer> si = new HashMap<>();
	
	public Encrypt(){
		Encrypt.is.put(2, "µ");
		Encrypt.is.put(3, "´");
		Encrypt.is.put(4, "`");
		Encrypt.is.put(5, "<");
		Encrypt.is.put(6, ">");
		Encrypt.is.put(7, "|");
		Encrypt.is.put(8, "'");
		Encrypt.is.put(9, "¸");
		Encrypt.is.put(10, "^");
		Encrypt.is.put(11, "ÿ");
		
		Encrypt.si.put("µ", 2);
		Encrypt.si.put("´", 3);
		Encrypt.si.put("`", 4);
		Encrypt.si.put("<", 5);
		Encrypt.si.put(">", 6);
		Encrypt.si.put("|", 7);
		Encrypt.si.put("'", 8);
		Encrypt.si.put("¸", 9);
		Encrypt.si.put("^", 10);
		Encrypt.si.put("ÿ", 11);
	}
	
	
	public static String encrypt(String s){
		String result = "";
		if(!s.startsWith("FILE:==:==:")){
		if(!s.startsWith("µ") & !s.startsWith("´") & !s.startsWith("`") & !s.startsWith("<") & !s.startsWith(">") & !s.startsWith("|") & !s.startsWith("'") & !s.startsWith("¸")
				 & !s.startsWith("^") & !s.startsWith("ÿ")){
		Random r = new Random();
		int n = r.nextInt(12);
		if(n < 2){
			n += 3;
		}
		for(char c: s.toCharArray()){
			int i = (int) c;
			if(c == ' '){
				result = result + "°";
			}else{
				i *= n;
				char a = (char)i;
				result = result + a;
			}
		}
		
		result =  is.get(n) + result;
		}
		}else{
			return s;
		}
		return result;
	}
	public static String decrypt(String s){
		
		String result = "";
		
		if(s.startsWith("µ") | s.startsWith("´") | s.startsWith("`") | s.startsWith("<") | s.startsWith(">") | s.startsWith("|") | s.startsWith("'") | s.startsWith("¸")
				 | s.startsWith("^") | s.startsWith("ÿ")){
		char[] ch = s.toCharArray();
		int n = si.get(String.valueOf(ch[0]));
		int j = 0;
		for(char c: s.toCharArray()){
			if(j != 0){
			int i = (int) c;
			if(c == '°'){
				result = result + " ";
			}else{
				i = i/n;
				char a = (char) i;
				result = result + a;
			}
			}else{
				j++;
			}
		}
		}else{
			return s;
		}
		
		return result;
	}
}
