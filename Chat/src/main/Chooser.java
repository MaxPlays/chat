package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import client.cgui;
import saver.Save;
import server.sgui;

public class Chooser extends JFrame {

	private static final long serialVersionUID = 6155992882412015424L;
	private JPanel contentPane;
	private JFrame frame;

	public Chooser(boolean b) {
		setResizable(false);
		setTitle("Chat R." + Chat.release);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 354, 133);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		
		frame = this;
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}
		
		JButton btnClient = new JButton("Client");
		btnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Save.loadFile();
				new cgui();
			}
		});
		btnClient.setBounds(12, 48, 122, 25);
		contentPane.add(btnClient);
		
		JButton btnServer = new JButton("Server");
		btnServer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				new sgui();
			}
		});
		btnServer.setBounds(180, 48, 122, 25);
		contentPane.add(btnServer);
		
		JLabel lblWasMchtestDu = new JLabel("Was m\u00F6chtest du starten?");
		lblWasMchtestDu.setBounds(12, 13, 324, 16);
		contentPane.add(lblWasMchtestDu);
		
		
		setVisible(true);
		
		if(b)
			JOptionPane.showMessageDialog(frame, "Das Update wurde erfolgreich installiert (R." + Chat.release + ")");
	}
}
