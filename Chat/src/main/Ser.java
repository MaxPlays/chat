package main;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class Ser {

	public static String toBase64(File file){
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(baos);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try {
			oos.writeObject(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
			return Base64.getEncoder().encodeToString(baos.toByteArray());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			baos.close();
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static byte[] fromBase64(String raw){
		ByteArrayInputStream bais = new ByteArrayInputStream(Base64.getDecoder().decode(raw));
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(bais);
			return (byte[]) ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			bais.close();
			ois.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
}
