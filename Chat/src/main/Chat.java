package main;

import server.Server;
import updater.Updater;

/**
* Created by Max_Plays on 16. Okt. 2016
*/
public class Chat {

	//IntelliJ edit

	public static int release = 1;
	public static String download = "https://bitbucket.org/MaxPlays/chat/downloads/Chat.jar";
	public static String version = "https://maxplays.bitbucket.io/chatversion.html";
	
	public static void main(String[] args) {
		
		new Updater();	
		
		if(args.length == 1){
			if(args[0].equalsIgnoreCase("-nogui")) {
				new Encrypt();
				new Server(false);
			}
		}else{
			new Encrypt();
			new Chooser(false);
		}
	
	}
	
}
